package com.capgemini.util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class DBConnectorTest {

	private DBConnector conn;
	
	@Before
	public void setUp() throws Exception {
		conn = new DBConnector("jdbc:oracle:thin:@localhost:1521:", "ALEXB", "SYS AS SYSDBA", "abance123");
	}

	@Test
	public void constructorTest() {
		assertThat("Test connection string is set properly", conn.getConnectionString(), equalTo("jdbc:oracle:thin:@localhost:1521:"));
		assertThat("Test sid is set properly", conn.getSID(), equalTo("ALEXB"));
		assertThat("Test username is set properly", conn.getUsername(), equalTo("SYS AS SYSDBA"));
		assertThat("Test password is set properly", conn.getPassword(), equalTo("abance123"));
	}
	
	@Test
	public void openConnectionTest() {
		assertThat("Test the connection is null before it has been opened", conn.getConnection(), nullValue());
		conn.openConnection();
		assertThat("Test the connection is not null after it has been opened", conn.getConnection(), notNullValue());
	}
	
	@Test
	public void closeConnectionTest() {
		conn.openConnection();
		assertThat("Test the connection is not null before it has been closed", conn.getConnection(), notNullValue());
		conn.closeConnection();
		assertThat("Test the connection is null after it has been closed", conn.getConnection(), nullValue());
	}
}
