package com.capgemini.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {

	private Connection connection;

	private String connectionString;
	private String sid;
	private String username;
	private String password;
	private boolean isOpen;

	public DBConnector(String connectionString, String sid, String username, String password) {
		this.connectionString = connectionString;
		this.sid = sid;
		this.username = username;
		this.password = password;
		isOpen = false;
	}

	public void openConnection() {
		if (connectionString != null && sid != null && username != null && password != null) {
			if (!isOpen) {
				try (Connection c = DriverManager.getConnection(connectionString + sid, username, password)) {
					connection = c;
					isOpen = true;
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		} else {
			throw new NullPointerException();
		}
	}

	public void closeConnection() {
		try {
			connection.close();
			isOpen = false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		connection = null;
	}

	public String getConnectionString() {
		return this.connectionString;
	}

	public String getSID() {
		return this.sid;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public Connection getConnection() {
		return this.connection;
	}
}
